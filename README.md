# All U Need Is Pizza

> _Made With **NodeJS** 12_

Isomorphic Projects For Testing Technologies With CRUD Pattern

## File Structure

```
.
+-- public
    +-- fonts
        +-- Nunito-Bold.ttf
        +-- Nunito-Regular.ttf
    +-- favicon.png
    +-- global.css
    +-- index.html
+-- src
    +-- components
        +-- containers
            +-- Add.svelte
            +-- index.js
            +-- Info.svelte
            +-- List.svelte
        +-- layers
            +-- Block.svelte
            +-- HyperLink.svelte
            +-- index.js
            +-- Radio.svelte
            +-- Row.svelte
            +-- TextField.svelte
        +-- App.svelte
    +-- services
        +-- pizza.js
    +-- utils
        +-- index.js
    +-- main.js
    +-- pizzas.json
+-- .gitignore
+-- LICENSE
+-- package.json
+-- README.md
+-- rollup.config.js
+-- yarn.lock
```

## Process

Repository:

```
git clone https://gitlab.com/aunip/svelte.git
```

Install:

```
npm install
```

Launch:

```
npm run start
```

Build:

```
npm run build
```

## License

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
